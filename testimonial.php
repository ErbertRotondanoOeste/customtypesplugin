<?php
/**
* Plugin Name: CPTs Oeste
* Description: Cria um plugin para inserir um os custom post types usados pela Oeste
* Version: 1.0
* Author: Oeste Tecnologia
* Author URI: http://oestetecnologia.com/
**/


add_action( 'init', 'register_cpts' );

function register_cpts() {
  // Cadastrndo Testimonials
  $labels = array(
    'name' => _x('Depoimentos', 'depoimentos'),
    'singular_name' => _x('Depoimento', 'depoimento'),
    'add_new' => _x('Adicionar novo', 'depoimento'),
    'add_new_item' => __('Adicionar novo depoimento'),
    'edit_item' => __('Editar Depoimento'),
    'new_item' => __('Novo Depoimento'),
    'all_items' => __('Todos os Depoimentos'),
    'view_item' => __('Visualizar Depoimento'),
    'search_items' => __('Buscar Depoimentos'),
    'not_found' =>  __('Nenhum depoimento encontrado'),
    'not_found_in_trash' => __('Nenhum depoimento encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Depoimentos')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'show_in_rest' => true,
    'supports' => array( 'title', 'editor', 'thumbnail')
  ); 

  register_post_type('testimonial',$args);

    // Cadastrando partners

    $labels = array(
        'name' => _x('Marcas', 'Marcas'),
        'singular_name' => _x('Marca', 'marca'),
        'add_new' => _x('Adicionar Nova', 'Marca'),
        'add_new_item' => __('Adicionar Nova Marca'),
        'edit_item' => __('Editar Marca'),
        'new_item' => __('Nova Marca'),
        'all_items' => __('Todas as Marcas'),
        'view_item' => __('Visualizar Marca'),
        'search_items' => __('Buscar Marcas'),
        'not_found' =>  __('Nenhuma Marca encontrada'),
        'not_found_in_trash' => __('Nenhuma Marca encontrada na lixeira'), 
        'parent_item_colon' => '',
        'menu_name' => __('Marcas')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => false,
        'menu_position' => null,
        'show_in_rest' => true,
        'supports' => array( 'title', 'editor', 'thumbnail')
    ); 

    // Cadastrando Brances

  register_post_type('partners', $args);
    $labels = array(
        'name' => _x('Filiais', 'filiais'),
        'singular_name' => _x('Filial', 'filial'),
        'add_new' => _x('Adicionar Nova', 'Filial'),
        'add_new_item' => __('Adicionar Nova Filial'),
        'edit_item' => __('Editar Filial'),
        'new_item' => __('Nova Filial'),
        'all_items' => __('Todas as Filiais'),
        'view_item' => __('Visualizar Filial'),
        'search_items' => __('Buscar Filiais'),
        'not_found' =>  __('Nenhuma Filial encontrada'),
        'not_found_in_trash' => __('Nenhuma Filial encontrada na lixeira'), 
        'parent_item_colon' => '',
        'menu_name' => __('Filiais')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => false,
        'menu_position' => null,
        'show_in_rest' => true,
        'supports' => array( 'title', 'editor', 'thumbnail')
    ); 

  register_post_type('branches', $args);
}

